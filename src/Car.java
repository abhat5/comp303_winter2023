public interface Car {
    String getMake();
    String getModel();
    int getYear();
}