import java.util.List;

public class Driver {
    public static void main(String[] args) {
        Dealership dealership = new Dealership();

        // Adding new cars to the dealership
        dealership.addCar(new NewCar("Toyota", "Camry", 2021));
        dealership.addCar(new NewCar("Honda", "Civic", 2022));
        dealership.addCar(new NewCar("Tesla", "Model 3", 2022));
        // Add more examples

        // Adding used cars to the dealership
        // Uncomment once UsedCar is implemented
        // dealership.addCar(new UsedCar("BMW", "X5", 2019, 100000));
        // dealership.addCar(new UsedCar("Mercedes-Benz", "GLC", 2020, 50000));
        // dealership.addCar(new UsedCar("Audi", "Q7", 2018, 80000));
        // Add more examples

        // Uncomment once UsedCar is implemented
        // List<UsedCar> usedCars = dealership.getUsedCars();
        // System.out.println("List of used cars:");
        // for (UsedCar usedCar : usedCars) {
        //    System.out.println("- " + usedCar.getMake() + " " + usedCar.getModel() + " " + usedCar.getYear() + " " + usedCar.getKilometersDriven());
        // }

        List<NewCar> newCars = dealership.getNewCars();
        System.out.println("List of new cars:");
        for (NewCar newCar: newCars) {
            System.out.println("- " + newCar.getMake() + " " + newCar.getModel() + " " + newCar.getYear());
        }

        // Demonstrate Strategy

    }
}
